#include <petriTransition.hpp>

std::ostream& petri::operator<<(std::ostream& os, petri::Transition t){
	os << "{";
	for (petri::slot n : t.inputs){
		auto firstMember = std::get<0>(n);
		auto secondMember = std::get<1>(n);
		os << firstMember << "/" << secondMember << " ";
	}
	os << "--> " << t.name << " -->";
	for (petri::slot n : t.outputs){
		auto firstMember = std::get<0>(n);
		auto secondMember = std::get<1>(n);
		os << " " << firstMember << "/" << secondMember;
	}
	os << "}";
	return os; 
}

bool petri::Transition::is_activable() const{
	bool out = true;
	for (petri::slot n : inputs){
		auto pool = std::get<0>(n);
		auto tokens = std::get<1>(n);
		out && !(pool < tokens);
	}
	return out;
}

void petri::Transition::activate(){
	for (auto [pool, tokens] : inputs){
		(pool -> nb_tokens) -= tokens; 
	}	
	for (auto [pool,tokens] : outputs){
		(pool-> nb_tokens) += tokens; 
	}
}		

petri::Transition::operator bool() const{
	return is_activable();
}

void petri::Transition::operator()(){
	return activate();
}
