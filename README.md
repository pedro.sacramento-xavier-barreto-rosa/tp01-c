# Petri Networks

Les réseaux de Petri sont un formalisme mathématique qui permet de spécifier des programmes où plusieurs tâches s'exécutent en parallèle, ces tâches devant parfois accéder à des ressources chacune à leur tour. Il est utilisé pour spécifier des protocoles de communication. Ce projet vise à réaliser une implémentation simple de leur comportement
